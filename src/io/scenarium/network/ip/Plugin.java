package io.scenarium.network.ip;

import io.beanmanager.PluginsBeanSupplier;
import io.scenarium.flow.consumer.OperatorConsumer;
import io.scenarium.flow.consumer.PluginsFlowSupplier;
import io.scenarium.network.ip.operator.network.bus.ethernet.SocketTCPClient;
import io.scenarium.network.ip.operator.network.bus.ethernet.SocketTCPServer;
import io.scenarium.network.ip.operator.network.bus.ethernet.SocketUDPClient;
import io.scenarium.network.ip.operator.network.bus.ethernet.SocketUDPServer;
import io.scenarium.pluginManager.PluginsSupplier;

public class Plugin implements PluginsSupplier, PluginsFlowSupplier, PluginsBeanSupplier {
	@Override
	public void populateOperators(OperatorConsumer operatorConsumer) {
		operatorConsumer.accept(SocketUDPClient.class);
		operatorConsumer.accept(SocketUDPServer.class);
		operatorConsumer.accept(SocketTCPClient.class);
		operatorConsumer.accept(SocketTCPServer.class);
	}

}

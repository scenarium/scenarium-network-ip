package io.scenarium.network.ip.operator.network.bus.ethernet;

import java.net.DatagramSocket;
import java.net.SocketException;

import io.beanmanager.editors.DynamicVisibleBean;

public class SocketUDPClient extends SocketUDP implements DynamicVisibleBean {

	public SocketUDPClient() {}

	public SocketUDPClient(DataType inDataType, DataType outDataType, boolean connectionFlag, boolean verbose, int readTimeout, int receiveBufferSize) {
		super(inDataType, outDataType, connectionFlag, verbose, readTimeout, receiveBufferSize);
	}

	@Override
	protected DatagramSocket createSocket() throws SocketException {
		return new DatagramSocket();
	}

	@Override
	protected boolean isOutputAvailable() {
		return false;
	}

	@Override
	public void setEnable() {}

	@Override
	public void setVisible() {
		fireSetPropertyVisible(this, "outDataType", false);
		fireSetPropertyVisible(this, "readTimeout", false);
		fireSetPropertyVisible(this, "receiveBufferSize", false);
	}
}

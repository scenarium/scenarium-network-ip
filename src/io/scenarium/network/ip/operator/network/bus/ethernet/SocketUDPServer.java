package io.scenarium.network.ip.operator.network.bus.ethernet;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.primitive.number.NumberInfo;

public class SocketUDPServer extends SocketUDP {
	@PropertyInfo(index = 0, nullable = false, info = "Local socket address to bind, or null for an unbound socket")
	private InetSocketAddress bindAddr;
	@PropertyInfo(index = 1, info = "Enable/disable the SO_REUSEADDR socket option")
	private boolean reuseAddress;
	@PropertyInfo(index = 2, info = "Sets the SO_SNDBUF option to the specified value for this DatagramSocket")
	@NumberInfo(min = 1)
	private int sendBufferSize;
	@PropertyInfo(index = 3, info = "Address of the machine from which the data should be received. Zero is interpreted as no filtering")
	private InetSocketAddress receivingAddress;
	@PropertyInfo(index = 4, info = "Sets traffic class or type-of-service octet in the IP datagram header for datagrams sent from this DatagramSocket."
			+ "\nAs the underlying network implementation may ignore this value applications should consider it a hint.")
	@NumberInfo(min = 0, max = 255)
	private int trafficClass;

	private boolean filterReceivingAddress;

	public SocketUDPServer() {
		try {
			this.bindAddr = new InetSocketAddress(InetAddress.getByName("0.0.0.0"), 0);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		try (DatagramSocket ds = new DatagramSocket(null)) {
			setReuseAddress(ds.getReuseAddress());
			setReceiveBufferSize(ds.getReceiveBufferSize());
			setSendBufferSize(ds.getSendBufferSize());
			setTrafficClass(ds.getTrafficClass());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public SocketUDPServer(DataType inDataType, DataType outDataType, boolean connectionFlag, boolean verbose, int readTimeout, int receiveBufferSize, InetSocketAddress bindAddr) {
		super(inDataType, outDataType, connectionFlag, verbose, readTimeout, receiveBufferSize);
		this.bindAddr = bindAddr;
	}

	@Override
	protected DatagramSocket createSocket() throws SocketException {
		DatagramSocket ds = new DatagramSocket(null);
		ds.setReuseAddress(getReuseAddress());
		ds.setReceiveBufferSize(getReceiveBufferSize());
		ds.setSendBufferSize(getSendBufferSize());
		ds.setTrafficClass(getTrafficClass());
		ds.bind(this.bindAddr);
		return ds;
	}

	public InetSocketAddress getBindAddr() {
		return this.bindAddr;
	}

	public void setBindAddr(InetSocketAddress bindAddr) {
		this.bindAddr = bindAddr;
		restartLater();
	}

	public boolean getReuseAddress() {
		return this.reuseAddress;
	}

	public void setReuseAddress(boolean reuseAddress) {
		this.reuseAddress = reuseAddress;
	}

	public InetSocketAddress getReceivingAddress() {
		return this.receivingAddress;
	}

	public void setReceivingAddress(InetSocketAddress receivingAddress) {
		this.receivingAddress = receivingAddress;
		boolean filterReceivingAddress = true;
		if (receivingAddress != null) {
			InetAddress add = receivingAddress.getAddress();
			if (add != null) {
				boolean zero = true;
				for (byte data : add.getAddress())
					if (data != 0) {
						zero = false;
						break;
					}
				if (zero)
					filterReceivingAddress = false;
			}
		}
		this.filterReceivingAddress = filterReceivingAddress;
	}

	public int getSendBufferSize() {
		return this.sendBufferSize;
	}

	public void setSendBufferSize(int sendBufferSize) {
		this.sendBufferSize = sendBufferSize;
	}

	public int getTrafficClass() {
		return this.trafficClass;
	}

	public void setTrafficClass(int trafficClass) {
		this.trafficClass = trafficClass;
	}

	@Override
	public void setEnable() {
		super.setEnable();
		fireSetPropertyEnable(this, "sendBufferSize", getOutDataType() != DataType.DISABLE);
		fireSetPropertyEnable(this, "receivingAddress", getOutDataType() != DataType.DISABLE);
		fireSetPropertyEnable(this, "trafficClass", getInDataType() != DataType.DISABLE);
	}

	@Override
	protected boolean canTrigger(DatagramPacket datagramPacket) {
		InetSocketAddress ra = this.receivingAddress;
		return ra == null || (!this.filterReceivingAddress || ra.getAddress().equals(datagramPacket.getAddress())) && (ra.getPort() == 0 || ra.getPort() == datagramPacket.getPort());
	}
}

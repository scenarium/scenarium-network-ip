package io.scenarium.network.ip.operator.network.bus.ethernet;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.primitive.number.NumberInfo;

public class SocketTCPServer extends SocketTCP {
	@PropertyInfo(index = 0, info = "The IP address and port number to bind to." + "A port number of zero means that it is automatically allocated."
			+ "If the address is null, then the system will pick up an ephemeral port and a valid local address to bind the socket")
	private InetSocketAddress endPoint;
	@PropertyInfo(index = 1, info = "Requested maximum length of the queue of incoming connections. A value of zero is interpreted as a default value")
	private int backlog = 0;
	@PropertyInfo(index = 2, unit = "ms", info = "The connection timeout. A timeout of zero is interpreted as an infinite timeout")
	@NumberInfo(min = 0)
	private int connectionTimeout;
	@PropertyInfo(index = 3, info = "Enable/disable the SO_REUSEADDR socket option")
	private boolean reuseAddress;
	private ServerSocket serveurSocket;

	public SocketTCPServer() {
		try (ServerSocket ss = new ServerSocket()) {
			setConnectionTimeout(ss.getSoTimeout());
			setReuseAddress(ss.getReuseAddress());
			setReceiveBufferSize(ss.getReceiveBufferSize());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public SocketTCPServer(DataType inDataType, DataType outDataType, boolean connectionFlag, boolean verbose, int readTimeout, int receiveBufferSize, int port, int backlog,
			InetSocketAddress endPoint) {
		super(inDataType, outDataType, connectionFlag, verbose, readTimeout, receiveBufferSize);
		this.endPoint = endPoint;
		this.backlog = backlog;
	}

	@Override
	protected Socket createSocket() throws IOException {
		if (this.serveurSocket != null)
			stop();
		this.serveurSocket = new ServerSocket();
		this.serveurSocket.setReuseAddress(this.reuseAddress);
		this.serveurSocket.setSoTimeout(this.connectionTimeout);
		this.serveurSocket.bind(this.endPoint, this.backlog);
		return this.serveurSocket.accept();
	}

	@Override
	protected synchronized void stop() {
		super.stop();
		if (this.serveurSocket != null) {
			try {
				this.serveurSocket.close();
			} catch (IOException e) {}
			this.serveurSocket = null;
		}
	}

	public InetSocketAddress getEndPoint() {
		return this.endPoint;
	}

	public void setEndPoint(InetSocketAddress endPoint) {
		this.endPoint = endPoint;
		restartLater();
	}

	public int getBacklog() {
		return this.backlog;
	}

	public void setBacklog(int backlog) {
		this.backlog = backlog;
		restartLater();
	}

	public int getConnectionTimeout() {
		return this.connectionTimeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public boolean isReuseAddress() {
		return this.reuseAddress;
	}

	public void setReuseAddress(boolean reuseAddress) {
		this.reuseAddress = reuseAddress;
	}
}

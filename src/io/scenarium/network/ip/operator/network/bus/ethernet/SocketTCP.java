/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.network.ip.operator.network.bus.ethernet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;

import io.scenarium.flow.ParamInfo;

public abstract class SocketTCP extends AbstractEthernet {
	private Socket socket;
	private ObjectOutputStream objectOutputStream;

	public SocketTCP() {}

	public SocketTCP(DataType inDataType, DataType outDataType, boolean connectionFlag, boolean verbose, int readTimeout, int receiveBufferSize) {
		super(inDataType, outDataType, connectionFlag, verbose, readTimeout, receiveBufferSize);
	}

	@ParamInfo(out = "Out")
	public void process() throws Exception {
		Object in = getAdditionalInputs()[0];
		Socket socket = this.socket;
		if (socket != null)
			try {
				if (getInDataType() == DataType.OBJECT) {
					if (this.objectOutputStream == null)
						this.objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
					this.objectOutputStream.writeObject(in);
					this.objectOutputStream.flush();
				} else if (in instanceof byte[] && getInDataType() == DataType.BYTE)
					socket.getOutputStream().write((byte[]) in);
				else if (in instanceof String && getInDataType() == DataType.STRING)
					socket.getOutputStream().write(((String) in).getBytes());
			} catch (IOException e) {
				if (isVerbose())
					System.err.println(getBlockName() + ": " + e.getMessage());
				restart();
			}
	}

	@Override
	protected synchronized void start() {
		death();
		this.ethernetThread = new Thread(() -> {
			byte[] receiveBuffer = null;
			DataType odt = getOutDataType();
			if (odt != DataType.DISABLE)
				receiveBuffer = new byte[getReceiveBufferSize()];
			while (isAlive()) {
				if (Thread.currentThread().isInterrupted()) {
					stop();
					return;
				}
				InputStream is = null;
				do {
					try {
						if (this.socket == null) {
							this.socket = createSocket();
							this.socket.setSoTimeout(getReadTimeout());
							if (odt != DataType.DISABLE)
								this.socket.setReceiveBufferSize(getReceiveBufferSize());
							connectionEstablished(true);
						}
						if (odt == DataType.DISABLE)
							return;
						is = this.socket.getInputStream();
					} catch (IOException e) {
						if (isVerbose())
							System.err.println(getBlockName() + ": " + e.getMessage());
						stop();
						if (!isAlive())
							return;
					}
					if (Thread.currentThread().isInterrupted()) {
						stop();
						return;
					}
				} while (is == null && isAlive());
				if (this.socket == null)
					return;
				try {
					if (odt == DataType.STRING)
						try (BufferedReader buf = new BufferedReader(new InputStreamReader(is))) {
							while (isAlive()) {
								if (Thread.currentThread().isInterrupted()) {
									stop();
									return;
								}
								String line = buf.readLine();
								if (line != null)
									triggerData(line);
								else
									throw new IOException("End of stream");
							}
						}
					else if (odt == DataType.BYTE)
						while (isAlive()) {
							if (Thread.currentThread().isInterrupted()) {
								stop();
								return;
							}
							int nbBytes = is.read(receiveBuffer);
							if (nbBytes > 0)
								triggerByteArrayData(receiveBuffer, nbBytes);
							else if (nbBytes == -1)
								throw new IOException("End of stream");
						}
					else if (odt == DataType.OBJECT) {
						ObjectInputStream objectOutput = new ObjectInputStream(is);
						while (isAlive()) {
							if (Thread.currentThread().isInterrupted()) {
								stop();
								return;
							}
							triggerData(objectOutput.readObject());
						}
					}
					stop();
				} catch (IOException | ClassNotFoundException e) {
					if (isAlive()) {
						if (isVerbose())
							System.err.println(getBlockName() + ": " + e.getMessage());
						if (!e.getClass().equals(SocketTimeoutException.class))
							stop();
					}
				}
			}
		}, getBlockName() + "-Receiver");
		this.ethernetThread.start();
	}

	protected abstract Socket createSocket() throws IOException;

	@Override
	protected synchronized void stop() {
		if (this.socket != null) {
			try {
				this.socket.close();
			} catch (IOException e) {}
			connectionEstablished(false);
			this.socket = null;
		}
		if (this.objectOutputStream != null) {
			try {
				this.objectOutputStream.close();
			} catch (IOException e) {}
			this.objectOutputStream = null;
		}
	}

	private void triggerByteArrayData(byte[] readBuff, int nbBytes) {
		byte[] datas = new byte[nbBytes];
		System.arraycopy(readBuff, 0, datas, 0, datas.length);
		triggerData(datas);
	}
}

package io.scenarium.network.ip.operator.network.bus.ethernet;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import io.beanmanager.editors.PropertyInfo;

public class SocketTCPClient extends SocketTCP {
	@PropertyInfo(index = 0, nullable = false, info = "The remote address and port")
	private InetSocketAddress address;
	@PropertyInfo(index = 1, info = "The local address the socket is bound to, or null for the anyLocal address and the local port the socket is bound to or zero for a system selected free port")
	private InetSocketAddress localAddr;

	public SocketTCPClient() {
		try {
			this.address = new InetSocketAddress(InetAddress.getByName("0.0.0.0"), 0);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		try (Socket socket = new Socket()) {
			setReceiveBufferSize(socket.getReceiveBufferSize());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public SocketTCPClient(DataType inDataType, DataType outDataType, boolean connectionFlag, boolean verbose, int readTimeout, int receiveBufferSize, InetSocketAddress address,
			InetSocketAddress localAddr) {
		super(inDataType, outDataType, connectionFlag, verbose, readTimeout, receiveBufferSize);
		this.address = address;
		this.localAddr = localAddr;
	}

	@Override
	protected Socket createSocket() throws IOException {
		InetSocketAddress add = this.address;
		InetSocketAddress lAdd = this.localAddr;
		return lAdd == null ? new Socket(add.getAddress(), add.getPort()) : new Socket(add.getAddress(), add.getPort(), lAdd.getAddress(), lAdd.getPort());
	}

	public InetSocketAddress getAddress() {
		return this.address;
	}

	public void setAddress(InetSocketAddress address) {
		this.address = address;
		restartLater();
	}

	public InetSocketAddress getLocalAddr() {
		return this.localAddr;
	}

	public void setLocalAddr(InetSocketAddress localAddr) {
		this.localAddr = localAddr;
		restartLater();
	}
}

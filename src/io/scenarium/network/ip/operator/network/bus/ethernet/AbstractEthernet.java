/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.network.ip.operator.network.bus.ethernet;

import io.beanmanager.editors.DynamicEnableBean;
import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.primitive.number.NumberInfo;

public abstract class AbstractEthernet extends AbstractBus implements DynamicEnableBean {
	@PropertyInfo(index = 50, unit = "ms", info = "Read timeout")
	@NumberInfo(min = 0)
	private int readTimeout = 1000;
	@PropertyInfo(index = 51, unit = "Byte", info = "Receive buffer size")
	@NumberInfo(min = 0)
	private int receiveBufferSize = 4098;

	protected Thread ethernetThread;

	public AbstractEthernet() {}

	public AbstractEthernet(DataType inDataType, DataType outDataType, boolean connectionFlag, boolean verbose, int readTimeout, int receiveBufferSize) {
		super(inDataType, outDataType, connectionFlag, verbose);
		this.readTimeout = readTimeout;
		this.receiveBufferSize = receiveBufferSize;
	}

	@Override
	public void birth() {
		onStart(() -> start());
	}

	@Override
	public void death() {
		if (this.ethernetThread != null) {
			this.ethernetThread.interrupt();
			try {
				this.ethernetThread.join();
				stop();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			this.ethernetThread = null;
		}
	}

	public int getReceiveBufferSize() {
		return this.receiveBufferSize;
	}

	public void setReceiveBufferSize(int receiveBufferSize) {
		this.receiveBufferSize = receiveBufferSize;
		restartLater();
	}

	public int getReadTimeout() {
		return this.readTimeout;
	}

	public void setReadTimeout(int readTimeout) {
		this.readTimeout = readTimeout;
		restartLater();
	}

	protected abstract void start();

	protected abstract void stop();

	@Override
	public void setInDataType(DataType inDataType) {
		super.setInDataType(inDataType);
		setEnable();
	}

	@Override
	public void setOutDataType(DataType outDataType) {
		super.setOutDataType(outDataType);
		setEnable();
	}

	@Override
	public void setEnable() {
		fireSetPropertyEnable(this, "readTimeout", getOutDataType() != DataType.DISABLE);
		fireSetPropertyEnable(this, "receiveBufferSize", getOutDataType() != DataType.DISABLE);
	}
}

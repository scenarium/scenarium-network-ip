/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.network.ip.operator.network.bus.ethernet;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import io.beanmanager.editors.PropertyInfo;

public abstract class SocketUDP extends AbstractEthernet {
	@PropertyInfo(index = 11, info = "The SocketAddress (usually IP address + port number) of the remote host to which this datagram is being sent")
	private InetSocketAddress destinationAddress;

	private DatagramSocket datagramSocket;

	public SocketUDP() {
		try (DatagramSocket datagramSocket = new DatagramSocket(null)) {
			setReceiveBufferSize(datagramSocket.getReceiveBufferSize());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public SocketUDP(DataType inDataType, DataType outDataType, boolean connectionFlag, boolean verbose, int readTimeout, int receiveBufferSize) {
		super(inDataType, outDataType, connectionFlag, verbose, readTimeout, receiveBufferSize);
	}

	public void process() {
		Object in = getAdditionalInputs()[0];
		DatagramSocket datagramSocket = this.datagramSocket;
		if (datagramSocket != null)
			try {
				byte[] sendBuf = null;
				if (getInDataType() == DataType.OBJECT) {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					try (ObjectOutputStream os = new ObjectOutputStream(baos)) {
						os.writeObject(in);
						os.flush();
						sendBuf = baos.toByteArray();
					}
				} else if (in instanceof byte[] && getInDataType() == DataType.BYTE)
					sendBuf = (byte[]) in;
				else if (in instanceof String && getInDataType() == DataType.STRING)
					sendBuf = ((String) in).getBytes();
				if (sendBuf != null)
					datagramSocket.send(new DatagramPacket(sendBuf, sendBuf.length, this.destinationAddress));
			} catch (IOException e) {
				if (isVerbose())
					System.err.println(getBlockName() + ": " + e.getMessage());
				restart();
			}
	}

	@Override
	protected synchronized void start() {
		death();
		this.ethernetThread = new Thread(() -> {
			byte[] receiveBuffer = null;
			byte[] outputDataBuffer = new byte[1];
			DataType odt = getOutDataType();
			if (odt != DataType.DISABLE)
				receiveBuffer = new byte[getReceiveBufferSize()];
			while (isAlive()) {
				if (Thread.currentThread().isInterrupted()) {
					stop();
					return;
				}
				do {
					try {
						if (this.datagramSocket == null) {
							this.datagramSocket = createSocket();
							if (odt != DataType.DISABLE)
								this.datagramSocket.setSoTimeout(getReadTimeout());
							connectionEstablished(true);
						}
						if (odt == DataType.DISABLE)
							return;
					} catch (IOException e) {
						if (isVerbose())
							System.err.println(getBlockName() + ": " + e.getMessage());
						if (!isAlive())
							return;
						stop();
					}
					if (Thread.currentThread().isInterrupted()) {
						stop();
						return;
					}
				} while (this.datagramSocket == null && isAlive());
				while (isAlive() && this.datagramSocket != null) {
					if (Thread.currentThread().isInterrupted()) {
						stop();
						return;
					}
					try {
						DatagramPacket dp = new DatagramPacket(receiveBuffer, receiveBuffer.length);
						this.datagramSocket.receive(dp);
						if (!canTrigger(dp))
							continue;
						if (outputDataBuffer.length != dp.getLength())
							outputDataBuffer = new byte[dp.getLength()];
						System.arraycopy(dp.getData(), dp.getOffset(), outputDataBuffer, 0, outputDataBuffer.length);
						if (odt == DataType.STRING)
							triggerData(new String(outputDataBuffer));
						else if (odt == DataType.BYTE)
							triggerData(outputDataBuffer);
						else if (odt == DataType.OBJECT)
							try (ObjectInputStream iStream = new ObjectInputStream(new ByteArrayInputStream(outputDataBuffer))) {
								triggerData(iStream.readObject());
							}
					} catch (IOException | NullPointerException | ClassNotFoundException e) {
						if (isAlive()) {
							if (isVerbose())
								System.err.println(getBlockName() + ": " + e.getMessage());
							if (!e.getClass().equals(SocketTimeoutException.class))
								stop();
						}
					}
				}
			}
		}, getBlockName() + "-Receiver");
		this.ethernetThread.start();
	}

	protected boolean canTrigger(DatagramPacket datagramPacket) {
		return true;
	}

	protected abstract DatagramSocket createSocket() throws SocketException;

	@Override
	protected synchronized void stop() {
		if (this.datagramSocket != null) {
			this.datagramSocket.close();
			connectionEstablished(false);
			this.datagramSocket = null;
		}
	}

	public InetSocketAddress getDestinationAddress() {
		return this.destinationAddress;
	}

	public void setDestinationAddress(InetSocketAddress destinationAddress) {
		this.destinationAddress = destinationAddress;
	}

	@Override
	public void setEnable() {
		super.setEnable();
		fireSetPropertyEnable(this, "destinationAddress", getInDataType() != DataType.DISABLE);
	}
}

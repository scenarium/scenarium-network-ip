import io.scenarium.network.ip.Plugin;
import io.scenarium.pluginManager.PluginsSupplier;

module io.scenarium.network.ip {
	uses PluginsSupplier;

	provides PluginsSupplier with Plugin;

	requires transitive io.scenarium.flow;

	exports io.scenarium.network.ip;
	exports io.scenarium.network.ip.operator.network.bus.ethernet;

}
